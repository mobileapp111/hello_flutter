import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String zuluGreeting = "Sawubona flutter";
String japaneseGreeting = "Kon'nichiwa Flutter";
String russianGreeting = "Privet flatter";


class _HelloFlutterAppState extends State<HelloFlutterApp> {

  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //Scaffold Widget
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    spanishGreeting:englishGreeting;
                  });
                }, icon: Icon(Icons.refresh)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    japaneseGreeting:englishGreeting;
                  });
                }, icon: Icon(Icons.arrow_circle_up_outlined)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    russianGreeting:englishGreeting;
                  });
                }, icon: Icon(Icons.arrow_circle_down_outlined))
          ],
        ),
        body: Center(
          child: Text(displayText,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}
